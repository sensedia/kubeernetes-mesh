#!/usr/bin/env bash

CURRENT_NAMESPACE=$(kubectl config get-contexts $(kubectl config current-context) | tail -n 1 | awk '{print $NF}')

find ./istio -type f -print0 | xargs -0 sed -i 's/<YOUR-NAME-HERE>\.sensedia\.com/'"$CURRENT_NAMESPACE"'.sensedia.com/g'
