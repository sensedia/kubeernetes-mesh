#!/usr/bin/env bash

CURRENT_NAMESPACE=$(kubectl config get-contexts $(kubectl config current-context) | tail -n 1 | awk '{print $NF}')
INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
BETS_HOST="$CURRENT_NAMESPACE.sensedia.com"

watch -n 0,1 "curl -X POST 'http://$BETS_HOST/api/bets' \
    --resolve $BETS_HOST:80:$INGRESS_HOST \
    --header 'Content-Type: application/json' \
    --data-raw '{}' \
    -H 'x-http-delay: yes' \
    -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1end2X21sVW82WFFvRjkxWVFXaFhsemRtaEIzZWNKLTFWXzhSeTZaUDBvIn0.eyJqdGkiOiIyOWFiOGI0ZS1kZjZmLTRkOWQtYTlmYi02MDIxNmVjYjA4NDUiLCJleHAiOjE1ODg4Mjg5OTcsIm5iZiI6MCwiaWF0IjoxNTg4ODAwMTk3LCJpc3MiOiJodHRwOi8vMzQuNzAuMjA0LjEyNi9hdXRoL3JlYWxtcy9jNC1pc3RpbyIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI1OWY0NjAzZS1lYTM0LTRmYWUtYjc1ZC0zZmNkNzMxOTkyNmMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJiZXQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI3OTQ3ZGUyZC03MzI4LTRkOTUtOGI3YS1mYWQzMzFhY2MyZmQiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InBsYXllcnM6cmVhZCBtYXRjaGVzOnJlYWQgYmV0OndyaXRlIGNoYW1waW9uc2hpcDpyZWFkIn0.gyrxarWm-txKu-KKjmfbssl9ItyA6sxdyDXth92Z6Z4JFE--FK8Jw5N8XXWj0gp9lInZAdqUifzH01Ay3fl3UbIThpRnTdwjtxmtzRkXyztt17G19NbTvNXnyFXuNeikI06cmtkcMG9C6_InEi5o3C5rXtRWaTA410WteEVUpNfSD-7033t0IRCJnrU8pvwrdU4mp_BsEFf8jXZyiL08cPazlCQEYRyUO4P_XSHGgiV-nx7kKPrr51ek4Kt3Yh_uvOEbf35VNLBxq9KjT2KNYLTtkzE4Ae4cEvIeXVROyIe5XY_jKS1Xrp23Fa9MRhIU95IFztnuAptMa_H_lbEyPg' \
    -s -w '\n%{time_total}s %{http_code}\n' "